export default {
  head: {
    title: 'how-to-vue-nuxt 🖖🏻',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },
  buildModules: [
    '@nuxtjs/eslint-module',
    '@nuxtjs/stylelint-module',
    '@nuxtjs/style-resources',
  ],
  css: ['~/assets/style/main.scss'],
  styleResources: {
    scss: ['~/assets/style/variables/variables-main.scss'],
  },
  plugins: [],
  components: false,
  modules: ['@nuxtjs/axios', 'nuxt-polyfill'],
  polyfill: {
    features: [
      {
        require: 'focus-visible',
      },
    ],
  },
  axios: {},
  build: {
    extend(config, ctx) {
      if (ctx.isClient) {
        config.devtool = 'source-map';
      }
    },
  },
  storybook: {},
};
