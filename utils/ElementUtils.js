export class ElementUtils {
  static buildRefGetter(name) {
    return {
      cache: false,
      get: function () {
        return { name, element: this.$refs[name] };
      },
    };
  }
}
